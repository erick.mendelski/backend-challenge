const express = require('express');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const axios = require('axios');


const typeDefs = `
    type Planet{
        name:String!
        mass:Float!
        hasStation: Boolean!
    }
    
    type Query{
        suitablePlanets: [Planet]
    }
    
    type Mutation {
        installStation(name: String!): Planet!
    }
`;
const app = express();

function suitablePlanets() {
    let planetList = [];

    axios.get(ARC).then(response => {
        let planetData = response.data['results'];
        for (let i in planetData) {
            let name = planetData[i]['name'];
            let mass = planetData[i]['mass'];
            if (mass) { mass = mass['value']; }

            if (mass >= 25) {
                console.log("Planeta: " + name + ' é apropiado');
                planetList.push({"name": name, "mass": mass, "hasStation": false});
            }
        }
    });
    console.log("\nPlanetas apropiados: " + planetList.length);
    return planetList;


}

const resolvers = {
    Query: {
        suitablePlanets() { return suitablePlanets() }
    },
    Mutation: {
        //installStation: (_, {name}) => planet.update({})
    }
};


const schema = makeExecutableSchema({typeDefs, resolvers});


app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

let port = 3050;
app.listen(port, () => {
    console.log('Go to http://localhost:'+port+'/graphiql to run queries!');
});
